
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indecies = @referee.check_guess(guess)
    update_board(indecies, guess)
    @guesser.handle_response(guess, indecies)
  end

  private

  def update_board(indecies, letter)
    indecies.each {|idx| @board[idx] = letter}
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    puts "Congratulations, you guessed the word. It was #{@board.join('')}"
  end

end

class HumanPlayer

  def initialize
    @guessed_letters = []
  end

  def register_secret_length(length)
    puts "The secret word has length #{length}"
  end

  def guess(board)
    print_board(board)
    print "> "
    guess = gets.chomp.downcase
    process_guess(guess)
  end

  def pick_secret_word
    print "Think of a secret word. Enter it's length"
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "The computer has guessed the letter '#{letter}'"
    print "Positions (ex. 0 2 4): "
    gets.chomp.split.map{|char| char.to_i - 1}
  end

  def handle_response(guess, indecies)

  end


  private

  def process_guess(guess)
    unless @guessed_letters.include?(guess)
      @guessed_letters << guess
      return guess
    else
      puts "You already guessed #{guess}"
      puts "You've already guessed: "
      p @guessed_letters
      puts "Please guess again"
      print ">"
      guess = gets.chomp.downcase
      process_guess(guess)
    end
  end

  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end.join('')
    puts "Secret word: #{board_string}"
  end
end

class ComputerPlayer
  # attr_reader :dictionary

  def self.read_dictionary
    File.readlines('lib/dictionary.txt').map(&:chomp)
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ('a'..'z').to_a
  end

  def candidate_words
    @dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indecies = []
    @secret_word.each_char.with_index do |char, idx|
      indecies << idx if char == letter
    end
    indecies
  end

  def register_secret_length(length)
    @dictionary.select! {|word| word.length == length}
  end

  def handle_response(letter, indecies)
    @dictionary.select! do |word|
      word_indecies = []
      word.chars.each_with_index do |char, idx|
        word_indecies << idx if char == letter
      end
      indecies == word_indecies
    end
  end

  def guess(board)
    best = @alphabet.first
    best_count = 0
    @alphabet.each do |letter|
      count = @dictionary.count {|word| word.include?(letter)}
      if count > best_count
        best = letter
        best_count = count
      end
    end
    @alphabet.delete(best)
    best
  end
end


if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    guesser: ComputerPlayer.new(dictionary),
    referee: HumanPlayer.new
  }

  game = Hangman.new(players)
  game.play
end
